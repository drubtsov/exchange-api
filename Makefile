APP_NAME = gin-app
APP_BIN = /tmp/$(APP_NAME).bin
PIDFILE = /tmp/$(APP_NAME).pid

default: restart
	@fswatch -o . | xargs -n1 -I{}  make restart || make kill

build:
	go build -o ${APP_BIN} *.go

kill:
	@kill `cat $(PIDFILE)` 2>/dev/null || true

restart: kill build
	$(APP_BIN) db up && $(APP_BIN) s & echo $$! > $(PIDFILE) && clear

.PHONY: build restart kill
