package migration

import (
	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

func init() {
	goose.SetLogger(h.Log)
}

func New(name string) {
	err := goose.Create(dao.PQ.DB(), "migration", name, "go")
	if err != nil {
		h.Log.Fatal(err)
	}
}

func Up() {
	err := goose.Up(dao.PQ.DB(), "migration")
	if err != nil {
		h.Log.Fatal(err)
	}
}

func Down() {
	err := goose.Down(dao.PQ.DB(), "migration")
	if err != nil {
		h.Log.Fatal(err)
	}
}
