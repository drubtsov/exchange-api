package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	"gitlab.com/drubtsov/exchange-api/models"
)

func init() {
	goose.AddMigration(Up00003, Down00003)
}

func Up00003(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	err := dao.PQ.CreateTable(&models.Family{}).Error
	if err != nil {
		return err
	}
	err = dao.PQ.Model(&models.Family{}).AddForeignKey("owner_id", "users(id)", "CASCADE", "CASCADE").Error
	if err != nil {
		return err
	}
	err = dao.PQ.Model(&models.Wallet{}).AddForeignKey("family_id", "families(id)", "CASCADE", "CASCADE").Error
	return err
}

func Down00003(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	err := dao.PQ.DropTable(&models.Family{}).Error
	return err
}
