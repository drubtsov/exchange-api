package migration

import (
	"database/sql"

	"github.com/pressly/goose"
	"gitlab.com/drubtsov/exchange-api/dao"
	"gitlab.com/drubtsov/exchange-api/models"
)

func init() {
	goose.AddMigration(Up00002, Down00002)
}

func Up00002(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	if err := dao.PQ.CreateTable(&models.Wallet{}).Error; err != nil {
		return err
	}
	err := dao.PQ.Model(&models.Wallet{}).AddForeignKey("owner_id", "users(id)", "CASCADE", "CASCADE").Error
	return err
}

func Down00002(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	err := dao.PQ.DropTable(&models.Wallet{}).Error
	return err
}
