package helpers

import "golang.org/x/crypto/bcrypt"

func GenerateHash(s string) string {
	digest, _ := bcrypt.GenerateFromPassword([]byte(s), bcrypt.DefaultCost)
	return string(digest)
}

func CompareHashAndString(passHash string, password string) error {
	err := bcrypt.CompareHashAndPassword([]byte(passHash), []byte(password))
	return err
}
