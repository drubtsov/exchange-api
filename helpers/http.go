package helpers

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetParamUint(c *gin.Context, param string) (uint64, error) {
	idString := c.Param(param)
	id, err := strconv.ParseUint(idString, 10, 64)
	return id, err
}
