package main

import (
	"os"

	"github.com/urfave/cli"
	"gitlab.com/drubtsov/exchange-api/controllers"
	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/migration"
)

func init() {
	h.LoadEnv()
}

func main() {
	// @title Exchange API
	// @version 0.1
	// @contact.email dmitry@rubtsov.eu

	app := cli.NewApp()
	app.Name = "exchange-api"
	app.Version = "0.1"

	app.Commands = []cli.Command{
		{
			Name:    "serve",
			Aliases: []string{"s"},
			Usage:   "Start server",
			Action: func(c *cli.Context) error {
				r := controllers.InitGin()
				h.Log.Info("Application started")
				h.Log.Fatal(r.Run(h.Env.ListenAddr))
				return nil
			},
		},
		{
			Name:    "database",
			Aliases: []string{"db"},
			Usage:   "Database migrations",
			Subcommands: []cli.Command{
				{
					Name:      "add",
					Usage:     "Add a new migration",
					ArgsUsage: "[migration name]",
					Action: func(c *cli.Context) error {
						if c.Args().First() == "" {
							h.Log.Fatal("Migration name is empty")
						}
						migration.New(c.Args().First())
						return nil
					},
				},
				{
					Name:  "up",
					Usage: "Migrate the DB to the most recent version available",
					Action: func(c *cli.Context) error {
						migration.Up()
						return nil
					},
				},
				{
					Name:  "down",
					Usage: "Roll back the version by 1",
					Action: func(c *cli.Context) error {
						migration.Down()
						return nil
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		h.Log.Fatal(err)
	}
}
