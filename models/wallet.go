package models

import (
	"errors"

	"gitlab.com/drubtsov/exchange-api/dao"
)

type Wallet struct {
	ID           uint          `json:"id" gorm:"primary_key;unique_index"`
	Name         string        `json:"name"`
	OwnerID      uint          `json:"owner_id"`
	FamilyID     *uint         `json:"family_id"`
	CurrencyID   *uint         `json:"currency_id"`
	Value        float64       `json:"value"`
	IsDeleted    bool          `json:"-" gorm:"default:'false'"`
	Transactions []Transaction `json:"-" gorm:"foreignkey:WalletID;association_foreignkey:ID"`
}

func (Wallet) TableName() string {
	return "wallets"
}

func (w Wallet) Create() (Wallet, error) {
	w.ID = 0
	err := dao.PQ.Create(&w).Error
	return w, err
}

func (w Wallet) Get() (Wallet, error) {
	err := dao.PQ.Where(&w).First(&w).Error
	return w, err
}

func (w Wallet) GetAll() ([]Wallet, error) {
	var wArr []Wallet
	err := dao.PQ.Where(&w).Find(&wArr).Error
	return wArr, err
}

func (w Wallet) Update() (Wallet, error) {
	err := dao.PQ.Save(&w).Error
	return w, err
}

func (w Wallet) Delete() (Wallet, error) {
	w.IsDeleted = true
	err := dao.PQ.Save(&w).Error
	return w, err
}

func (w Wallet) GetUsers() ([]User, error) {
	var users []User
	var err error
	if w.FamilyID != nil {
		users, err = Family{ID: *w.FamilyID}.GetUsers()
	} else {
		owner, err := User{ID: w.OwnerID}.Get()
		if err != nil {
			return nil, errors.New("owner_id is empty")
		}
		users = append(users, owner)
	}
	return users, err
}
