package models

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/drubtsov/exchange-api/dao"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

type User struct {
	ID            uint     `json:"id" gorm:"primary_key;unique_index" example:"1337"`
	Email         string   `json:"email" gorm:"unique_index" example:"satoshin@gmx.com"`
	PassHash      string   `json:"-"`
	Role          string   `json:"-" gorm:"default:'user'"`
	IsDeleted     bool     `json:"-" gorm:"default:'false'"`
	IsConfirmed   bool     `json:"-" gorm:"default:'false'"`
	Wallets       []Wallet `json:"-" gorm:"foreignkey:OwnerID;association_foreignkey:ID"`
	FamiliesOwner []Family `json:"-" gorm:"foreignkey:OwnerID;association_foreignkey:ID"`
	Families      []Family `json:"-" gorm:"many2many:user_families;"`
}

func (User) TableName() string {
	return "users"
}

func (u User) New() (User, error) {
	err := dao.PQ.Save(&u).Error
	return u, err
}

func (u User) Update() (User, error) {
	err := dao.PQ.Save(&u).Error
	return u, err
}

func (u User) Delete() (User, error) {
	u.IsDeleted = true
	err := dao.PQ.Save(&u).Error
	return u, err
}

func (u User) Get() (User, error) {
	err := dao.PQ.Where(&u).First(&u).Error
	if u.IsDeleted == true {
		return u, errors.New("user deleted")
	}
	return u, err
}

func (u User) GetFamilies() ([]Family, error) {
	var fArr []Family
	err := dao.PQ.Model(&u).Association("Families").Find(&fArr).Error
	return fArr, err
}

func (u User) AuthorizeForRequest(req *http.Request) {
	token := h.MakeAuthToken(fmt.Sprint(u.ID), u.Role).Token
	req.Header.Add("Authorization", "Bearer "+token)
}
