package controllers

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/models"
)

type AuthForm struct {
	Email    string `form:"email" json:"email" binding:"required" example:"satoshin@gmx.com"`
	Password string `form:"password" json:"password" binding:"required" example:"qazwsxedc"`
}

type TokenForm struct {
	Token string `form:"token" json:"token" binding:"required" example:"eyJhb.eyJleHAiO.7XJEAvxHk"`
}

type authControllers struct{}

var Auth authControllers

// @Summary Login
// @ID login
// @Accept  json
// @Produce  json
// @Param Authorization body controllers.AuthForm true "Auth Form"
// @Success 200 {object} helpers.JWTResponse
// @Failure 401 {object} helpers.HTTPError
// @Router /auth/login [post]
func (*authControllers) Login(c *gin.Context) {
	var form AuthForm
	if err := c.BindJSON(&form); err != nil {
		h.NewError(c, 400, err, "invalid login")
		return
	}
	user, err := models.User{Email: form.Email}.Get()
	if err != nil || user.IsDeleted != false || user.IsConfirmed != true {
		h.NewError(c, 401, err, "user not found")
		return
	}

	err = h.CompareHashAndString(user.PassHash, form.Password)
	if err != nil {
		h.NewError(c, 401, err, "incorrect password")
		return
	}
	c.JSON(200, h.MakeAuthToken(strconv.FormatUint(uint64(user.ID), 10), user.Role))
}

// @Summary Confirming user email
// @ID confirm-email
// @Produce  json
// @Param Authorization body controllers.TokenForm true "Token Form"
// @Success 200 {object} helpers.HTTPStatus
// @Failure 400 {object} helpers.HTTPError
// @Failure 401 {object} helpers.HTTPError
// @Router /auth/confirmation [post]
func (*authControllers) Confirm(c *gin.Context) {
	var form TokenForm
	if err := c.BindJSON(&form); err != nil {
		h.NewError(c, 400, err, "invalid request")
		return
	}
	email, err := h.CheckEmailVerificationToken(form.Token)
	if err != nil {
		h.NewError(c, 401, err, "invalid token")
		return
	}
	user, err := models.User{Email: email}.Get()
	if err != nil || user.IsDeleted != false || user.IsConfirmed == true {
		h.NewError(c, 401, err, "token already used")
		return
	}
	user.IsConfirmed = true
	user.Update()
	h.NewStatus(c, "confirmed")
}

// @Summary Register
// @ID register
// @Accept  json
// @Produce  json
// @Param Authorization body controllers.AuthForm true "Auth Form"
// @Success 200 {object} models.User
// @Failure 401 {object} helpers.HTTPError
// @Failure 500 {object} helpers.HTTPError
// @Router /auth/register [post]
func (*authControllers) Register(c *gin.Context) {
	var form AuthForm
	if err := c.BindJSON(&form); err != nil {
		h.NewError(c, 400, err, "invalid login")
		return
	}
	user, _ := models.User{Email: form.Email}.Get()
	if user.ID != 0 {
		h.NewError(c, 401, errors.New("user already exist"), "user already exist")
		return
	}
	passHash := h.GenerateHash(form.Password)

	user, _ = models.User{
		Email:    form.Email,
		PassHash: passHash,
		Role:     "user",
	}.New()

	go h.EmailSender.DialAndSend(h.RegistrationEmail(user.Email))

	c.JSON(200, user)
}
