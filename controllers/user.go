package controllers

import (
	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/models"
)

type userControllers struct{}

var User userControllers

// @Summary Get user
// @ID get-user
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param id path int true "User ID"
// @Success 200 {object} models.User
// @Failure 400 {object} helpers.HTTPError
// @Failure 403 {object} helpers.HTTPError
// @Failure 404 {object} helpers.HTTPError
// @Router /users/{id} [get]
func (*userControllers) Get(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	if h.GetUintID(c) != uint(id) {
		h.NewError(c, 403, err, "permission denied")
		return
	}
	user, _ := models.User{ID: h.GetUintID(c)}.Get()
	c.JSON(200, user)
}

// @Summary Delete user
// @ID delete-user
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param id path int true "User ID"
// @Success 200 {object} helpers.HTTPStatus
// @Failure 401 {object} helpers.HTTPError
// @Failure 403 {object} helpers.HTTPError
// @Failure 500 {object} helpers.HTTPError
// @Router /users/{id} [delete]
func (*userControllers) Delete(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	if h.GetUintID(c) != uint(id) {
		h.NewError(c, 403, err, "permission denied")
	}
	user, _ := models.User{ID: h.GetUintID(c)}.Get()

	user.Delete()
	h.NewStatus(c, "deleted")
}
