package controllers

import (
	"github.com/gin-gonic/gin"
	h "gitlab.com/drubtsov/exchange-api/helpers"
	"gitlab.com/drubtsov/exchange-api/models"
)

type currencyControllers struct{}

var Currency currencyControllers

// @Summary Create currency
// @ID create-currency
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param Wallet body models.Currency true "Currency model"
// @Success 200 {object} models.Currency
// @Failure 401 {object} helpers.HTTPError
// @Router /currencies [post]
func (*currencyControllers) Create(c *gin.Context) {
	var currency models.Currency
	if err := c.BindJSON(&Currency); err != nil {
		h.NewError(c, 400, err, "invalid currency")
		return
	}
	currency, _ = currency.Create()

	//TODO check if already exist
	c.JSON(200, currency)
}

// @Summary Get all currencies
// @ID get-all-currencies
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Success 200 {array} models.Currency
// @Failure 404 {object} helpers.HTTPError
// @Router /currencies [get]
func (*currencyControllers) GetAll(c *gin.Context) {
	currencies, err := models.Currency{ID: uint(h.GetUintID(c))}.GetAll()
	if err != nil || len(currencies) == 0 {
		h.NewError(c, 404, err, "currencies not found")
		return
	}
	c.JSON(200, currencies)
}

// @Summary Delete currency
// @ID delete-currency
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Authentication header"
// @Param id path int true "Currency ID"
// @Success 200 {object} models.Currency
// @Failure 400 {object} helpers.HTTPError
// @Failure 403 {object} helpers.HTTPError
// @Router /families/{id} [delete]
func (*currencyControllers) Delete(c *gin.Context) {
	id, err := h.GetParamUint(c, "id")
	if err != nil {
		h.NewError(c, 400, err, "invalid id")
		return
	}
	currency, err := models.Currency{ID: uint(id)}.Get()
	if err != nil {
		h.NewError(c, 404, err, "currencies not found")
		return
	}
	if err = currency.Delete(); err != nil {
		h.NewError(c, 500, err, "deleting error")
		return
	}
	h.NewStatus(c, "deleted")
}
