package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/drubtsov/exchange-api/models"
)

func TestWalletCreate(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet := models.Wallet{
		Name:    fake.CreditCardType(),
		OwnerID: u.ID,
	}
	jsonWallet, _ := json.Marshal(wallet)

	w := PerformRequest(
		"POST",
		"/wallets",
		u,
		bytes.NewBuffer(jsonWallet),
	)

	createdWallet, _ := models.Wallet{
		OwnerID: u.ID,
		Name:    wallet.Name,
	}.Get()

	jsonWallet, err := json.Marshal(createdWallet)

	assert.Nil(t, err)
	assert.Equal(t, string(jsonWallet), w.Body.String())
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestWalletCreateInvalidBody(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	jsonWallet := []byte("invalid_body")

	w := PerformRequest(
		"POST",
		"/wallets",
		u,
		bytes.NewBuffer(jsonWallet),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestWalletGet(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet, _ := models.Wallet{
		Name:    fake.CreditCardType(),
		OwnerID: u.ID,
	}.Create()

	jsonWallet, _ := json.Marshal(wallet)

	w := PerformRequest(
		"GET",
		"/wallets/"+fmt.Sprint(wallet.ID),
		u,
		nil,
	)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, string(jsonWallet), w.Body.String())
}

func TestWalletGetNotFound(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	u2, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet, _ := models.Wallet{
		Name:    fake.CreditCardType(),
		OwnerID: u.ID,
	}.Create()

	w := PerformRequest(
		"GET",
		"/wallets/"+fmt.Sprint(wallet.ID),
		u2,
		nil,
	)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestWalletGetInvalidID(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	id := "invalid_id"

	w := PerformRequest(
		"GET",
		"/wallets/"+id,
		u,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestWalletGetAll(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	var wArr [3]models.Wallet
	for i := 0; i < 3; i++ {
		wArr[i], _ = models.Wallet{
			Name:    fake.CreditCardType(),
			OwnerID: u.ID,
		}.Create()
	}

	w := PerformRequest(
		"GET",
		"/wallets",
		u,
		nil,
	)

	jsonCreatedWallets, err := json.Marshal(wArr[:])

	assert.Nil(t, err)
	assert.Equal(t, string(jsonCreatedWallets), w.Body.String())
	assert.Equal(t, http.StatusOK, 200)
}

func TestWalletUpdate(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet := models.Wallet{
		Name:    fake.Word(),
		OwnerID: u.ID,
	}

	createdWallet, _ := wallet.Create()

	wallet.Name = fake.Word()
	wallet.ID = createdWallet.ID

	jsonWallet, _ := json.Marshal(wallet)

	w := PerformRequest(
		"PUT",
		"/wallets/"+fmt.Sprint(wallet.ID),
		u,
		bytes.NewBuffer(jsonWallet),
	)

	updatedWallet, _ := models.Wallet{ID: wallet.ID}.Get()

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEqual(t, createdWallet.Name, updatedWallet.Name, "they shouldn't be equal")
}

func TestWalletUpdateInvalidID(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	id := "invalid_id"

	w := PerformRequest(
		"PUT",
		"/wallets/"+id,
		u,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestWalletUpdateInvalidBody(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	jsonWallet := []byte("invalid_body")

	w := PerformRequest(
		"PUT",
		"/wallets/5",
		u,
		bytes.NewBuffer(jsonWallet),
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestWalletUpdateNotOwner(t *testing.T) {
	owner, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	user, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet := models.Wallet{
		Name:    fake.Word(),
		OwnerID: owner.ID,
	}

	createdWallet, _ := wallet.Create()

	wallet.Name = fake.Word()
	wallet.ID = createdWallet.ID

	jsonWallet, _ := json.Marshal(wallet)

	w := PerformRequest(
		"PUT",
		"/wallets/"+fmt.Sprint(wallet.ID),
		user,
		bytes.NewBuffer(jsonWallet),
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestWalletDelete(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet, _ := models.Wallet{
		Name:    fake.CreditCardType(),
		OwnerID: u.ID,
	}.Create()

	w := PerformRequest(
		"DELETE",
		"/wallets/"+fmt.Sprint(wallet.ID),
		u,
		nil,
	)

	wallet, err := wallet.Get()

	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, wallet.IsDeleted, true)
}

func TestWalletDeleteInvalidID(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	id := "invalid_id"

	w := PerformRequest(
		"DELETE",
		"/wallets/"+id,
		u,
		nil,
	)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestWalletDeleteNotFound(t *testing.T) {
	u, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet, _ := models.Wallet{
		Name:    fake.CreditCardType(),
		OwnerID: u.ID,
	}.Create()

	w := PerformRequest(
		"DELETE",
		"/wallets/"+fmt.Sprint(wallet.ID+1),
		u,
		nil,
	)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestWalletDeleteNotOwner(t *testing.T) {
	owner, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	user, _ := models.User{
		Email:    fake.EmailAddress(),
		PassHash: fake.SimplePassword(),
	}.New()

	wallet, _ := models.Wallet{
		Name:    fake.CreditCardType(),
		OwnerID: owner.ID,
	}.Create()

	w := PerformRequest(
		"DELETE",
		"/wallets/"+fmt.Sprint(wallet.ID),
		user,
		nil,
	)

	assert.Equal(t, http.StatusForbidden, w.Code)
}
