package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/drubtsov/exchange-api/dao"
	h "gitlab.com/drubtsov/exchange-api/helpers"
)

// @Summary Get Application Status
// @Produce  json
// @Success 200 {object} helpers.HTTPStatus
// @Failure 500 {object} helpers.HTTPError
// @Router /status [get]
func GetAppStatus(c *gin.Context) {
	err := dao.PQ.DB().Ping()
	if err != nil {
		h.NewError(c, 500, err, "server error")
		return
	}
	h.NewStatus(c, "ok")
}
