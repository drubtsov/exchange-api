package controllers

import (
	"errors"
	"time"

	h "gitlab.com/drubtsov/exchange-api/helpers"

	"github.com/gin-gonic/contrib/ginrus"
	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"gitlab.com/drubtsov/exchange-api/middleware"

	_ "gitlab.com/drubtsov/exchange-api/docs"
)

// Initialize Gin engine
func InitGin() *gin.Engine {
	// New Gin Instance
	r := gin.New()

	// Setup logger
	r.Use(ginrus.Ginrus(h.Log, time.RFC3339, true))

	// Setup CORS middleware
	r.Use(cors.Middleware(cors.Config{
		Origins:         "*", //TODO
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     true,
		ValidateHeaders: false,
	}))

	// Middleware for check if server working properly
	r.Use(middleware.SystemCheck())

	// Handle 404
	r.NoRoute(func(c *gin.Context) {
		h.NewError(c, 404, errors.New("invalid route"), "route not found")
	})

	// Application status
	r.GET("/status", GetAppStatus)

	// Documentation
	r.GET("/", docsRedirect)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Auth
	auth := r.Group("/auth")
	{
		auth.POST("confirmation", Auth.Confirm)
		auth.POST("login", Auth.Login)
		auth.POST("register", Auth.Register)
	}
	// Users
	users := r.Group("/users", middleware.JWT(), middleware.Role.User())
	{
		users.GET(":id", User.Get)
		users.DELETE(":id", User.Delete)
	}
	// Wallets
	wallets := r.Group("/wallets", middleware.JWT(), middleware.Role.User())
	{
		wallets.GET("", Wallet.GetAll)
		wallets.GET(":id", Wallet.Get)
		wallets.POST("", Wallet.Create)
		wallets.PUT(":id", Wallet.Update)
		wallets.DELETE(":id", Wallet.Delete)
	}
	// Families
	families := r.Group("/families", middleware.JWT(), middleware.Role.User())
	{
		families.GET("", Family.GetAll)
		families.GET(":id", Family.Get)
		families.GET(":id/users", Family.GetUsers)
		families.POST("", Family.Create)
		families.POST(":id/users", Family.AddUser)
		families.POST(":id", Family.ConfirmUser)
		families.PUT(":id", Family.Update)
		families.DELETE(":id", Family.Delete)
		families.DELETE(":id/users/:user_id", Family.DeleteUser)
	}
	// Currency
	currencies := r.Group("/currencies", middleware.JWT(), middleware.Role.User())
	{
		currencies.GET("", Currency.GetAll)
		currencies.POST("", middleware.Role.Admin(), Currency.Create)
		currencies.DELETE(":id", middleware.Role.Admin(), Currency.Delete)
	}

	return r
}

func docsRedirect(c *gin.Context) {
	c.Redirect(301, "/swagger/index.html")
}
